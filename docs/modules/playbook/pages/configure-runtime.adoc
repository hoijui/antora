= Runtime Configuration

On this page, you'll learn:

* [x] How to configure the cache directory.
* [x] How to fetch updates to content sources and UI.

[#cache]
== Cache directory

The first time Antora runs, it caches any remote git repositories and UI bundles.
On subsequent runs, Antora then resolves the remote resources from this cache instead.
The remote resources are stored in Antora's default cache location unless an alternate location is configured in the playbook using the `cache_dir` key.

[#default-cache]
=== Default cache directory

The default location for the cache varies by operating system.

* Linux: [.path]_$XDG_CACHE_HOME/antora_ (or [.path]_$HOME/.cache/antora_ if `$XDG_CACHE_HOME` is not set)
* macOS: [.path]_$HOME/Library/Caches/antora_
* Windows: [.path]_$APPDATA/antora/Caches_

Before downloading remote resources, Antora will first look for those resources in the cache folder, which maps to the user's cache folder by default.
If you want to instruct Antora to update the cache, configure Antora to <<fetch,fetch updates>>.
Another option is to locate the Antora cache directory on your system and delete it.

[#cache-dir]
=== Specify a cache directory

The cache directory can be specified using the `cache_dir` key under the `runtime` category in the playbook.
The key specifies the directory where the remote repositories should be cloned and the remote UI bundle should be downloaded.
The key accepts a relative or absolute filesystem path.

[source,yaml]
----
runtime:
  cache_dir: ./.cache/antora
----

In this case, the value resolves to the folder [.path]_.cache/antora_ relative to the location of the playbook file.
We know the the resolved location is relative to the playbook file because it starts with `./`.

The resolution rules for `cache-dir` are the same as for any path in the playbook.
A relative path is expanded to an absolute path using the following rules:

* If the first path segment is a tilde (`~`), the remaining path is resolved relative to the user's home directory.
* If the first path segment is a dot (`.`), the remaining path is resolved relative to the location of the playbook file.
* If the first path segment is tilde plus (`~+`), or does not begin with an aforementioned prefix, the remaining path is resolved relative to the current working directory.

[#fetch]
== Fetch updates

The first time Antora runs, it caches any remote git repositories and UI bundles.
On subsequent runs, Antora looks for these resources in the cache folder, effectively running offline.
You can tell Antora to refresh the cache by setting the `fetch` key under the `runtime` category to `true`.

[source,yaml]
----
runtime:
  fetch: true
----

However, you don't have to modify the playbook file directly to set this key.
You can use the `--fetch` CLI switch.

 $ antora --fetch antora-playbook.yml

The `--fetch` CLI switch sets the `fetch` key to `true`, overriding any value set in the playbook file.
Setting the `fetch` key to true activates two behaviors in Antora:

. Run a fetch operation on all cloned repositories (content sources that are remote)
. Download the remote UI bundle again if it's marked as a xref:configure-ui.adoc#snapshot[snapshot]

Use `fetch` whenever you want to retrieve updates to the remote content sources and UI bundle snapshots.
